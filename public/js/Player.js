class Player {
  constructor() {
    this.position = {
      x: 0,
      y: 0
    }
  
    this.topLeftPoint = {
      x: undefined,
      y: undefined
    }
  
    this.bottomRightPoint = {
      x: undefined,
      y: undefined
    }
  
    this.size = {
      width: PLAYER_WIDTH,
      height: PLAYER_HEIGHT
    }
      
    this.color = color(random(255), random(255), random(255));
  
    this.velocity = {
      x: PLAYER_START_VELOCITY_X,
      y: PLAYER_START_VELOCITY_Y
    }
  
    this.jumpAcceleration = {
      x: 0,
      y: 5
    }
  
    this.isDynamic = true;

    this.canJump = true;

    this.setEdges();
    this.moveToStartingPoint();
  }
  
  setEdges() {
    this.topLeftPoint.x = this.position.x - this.size.width/2;
    this.topLeftPoint.y = this.position.y + this.size.height/2;
   
    this.bottomRightPoint.x = this.position.x + this.size.width/2;
    this.bottomRightPoint.y = this.position.y - this.size.height/2;
  }

  get projection() {
    return {
      x: (this.position.x - camera.position.x) * camera.zoom,
      y: (this.position.y - camera.position.y) * camera.zoom,
      width: this.size.width * camera.zoom,
      height: this.size.height * camera.zoom,
    }
  }

  jump() {
    if (this.canJump === true) {
      this.velocity.y += this.jumpAcceleration.y;
      this.canJump = false;
    }
  }

  checkCollisions() {
    for (let obstacle of world.obstacles) {
      if (gameManager.drawCollisionLines === true) {
        this.drawCollisionLines(obstacle);
      }

      if (this.isCollidingWithObstacle(obstacle) === true) {
          gameManager.gameEnd();
      }
    }

    for (let coin of world.coins) {
      if (gameManager.drawCollisionLines === true) {
        this.drawCollisionLines(coin);
      }

      if (this.isCollidingWithCoin(coin) === true) {
        if (coin.type === 'speedUp') {
          this.speedUp(PLAYER_COIN_ACCELERATION_MULTIPLIER);
        } else if(coin.type === 'slowDown') {
          this.slowDown(PLAYER_COIN_DEACCELERATION_MULTIPLIER);
        }

        world.removeCollectedCoin(coin);
      }
    }
  }

  speedUp(multiplier = 1) {
    this.velocity.x += PLAYER_ACCELERATION * multiplier;

    if (this.velocity.x > PLAYER_MAX_VELOCITY_X) 
      this.velocity.x = PLAYER_MAX_VELOCITY_X;    

    camera.zoomOut();
  }

  slowDown(multiplier = 1) {
    this.velocity.x -= PLAYER_ACCELERATION * multiplier;

    if (this.velocity.x < PLAYER_MIN_VELOCITY_X) 
      this.velocity.x = PLAYER_MIN_VELOCITY_X;

    camera.zoomIn();
  }

  drawCollisionLines(obstacle) {
    push();
    stroke(255, 0, 0);
    line(this.projection.x, this.projection.y, obstacle.projection.x, obstacle.projection.y);
    pop();
  }

  isCollidingWithObstacle(obstacle) {
    if (obstacle.topLeftPoint.x > this.bottomRightPoint.x || this.topLeftPoint.x > obstacle.bottomRightPoint.x)
        return false;

    if (obstacle.topLeftPoint.y < this.bottomRightPoint.y || this.topLeftPoint.y < obstacle.bottomRightPoint.y)
        return false;

    return true;
  }

  isCollidingWithCoin(coin) {
    return coin.position.x > this.topLeftPoint.x && 
    coin.position.x < this.bottomRightPoint.x && 
    coin.position.y > this.bottomRightPoint.y && 
    coin.position.y < this.topLeftPoint.y;
  }

  update(acceleration) {
    this.velocity.x += acceleration.x;
    this.velocity.y += acceleration.y;

    this.position.x += this.velocity.x;
    this.position.y += this.velocity.y;

    this.setEdges();

    if (gameManager.playerChecksCollisions === true)
      this.checkCollisions();

    if (this.isCollidingWithPlatform() === true) {
      this.resetPositionToGround();
    }

    if (this.isAtPlatformEnd() === true) {
      gameManager.levelCompleted();
    }
  }

  resetPositionToGround() {
    this.position.y = platform.position.y + this.size.height/2;
    this.velocity.y = 0;
    this.canJump = true;
  }
  
  isAtPlatformEnd() {
    return this.position.x >= (platform.size.length/2 + width/2/camera.zoom);
  }

  isCollidingWithPlatform() {
    return (this.position.y - this.size.height/2) < platform.position.y;
  }

  moveToStartingPoint() {
    this.position.x = -platform.size.length/2 - width/2/camera.zoom;
  }

  draw() {
    fill(this.color);
    rect(this.projection.x, this.projection.y, this.projection.width, this.projection.height);

    if (mouseIsPressed === false && gameManager.cameraFollowsPlayer === true)
      camera.setPosition(this.position.x, this.position.y);
  }
}