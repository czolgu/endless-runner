class BrowserCookies {
  static SESSION_COOKIE_NAME = 'sid';

  static set(cookieName, cookieValue, exdays) {
    let date = new Date();
    date.setTime(date.getTime() + (exdays*24*60*60*1000));

    let expires = `expires=${date.toUTCString()}`;
    document.cookie = `${cookieName}=${cookieValue};${expires};path=/`;
  }

  static get(cookieName) {
      let name = cookieName + '=';
      let decodedCookie = decodeURIComponent(document.cookie);
      var cookieArray = decodedCookie.split(';');

      for (let i = 0; i < cookieArray.length; i++) {
        let cookieComponent = cookieArray[i];

        while (cookieComponent.charAt(0) == ' ') {
          cookieComponent = cookieComponent.substring(1);
        }

        if (cookieComponent.indexOf(name) == 0) {
          return cookieComponent.substring(name.length, cookieComponent.length);
        }
      }
      return '';
  }
}