class CanvasManager {
  static prepareCanvas() {
    canvasWidth  = windowWidth;
    canvasHeight = windowHeight;
    canvas = createCanvas(canvasWidth, canvasHeight);
    canvas.parent('canvas-container');
  }
}