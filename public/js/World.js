class World {
  constructor() {
    this.dynamicObjects = [];
    this.staticObjects = [];
    this.forces = [];
    this.obstacles = [];
    this.coins = [];
  }

  setup() {
    this.createGravityForce();
    this.createPlatform();
    this.createBackground();
    this.createPlayer();
    this.createObstacles();
    this.createCoins();
  }

  addForce(force) {
    this.forces = [...this.forces, force];
  }

  createGravityForce() {
    gravity = {
      x: 0,
      y: GRAVITY_ACCELERATION,
    }
    this.addForce(gravity);
  }

  createPlatform() {
    platform = new Platform();
    this.add(platform);
  }

  createPlayer() {
    player = new Player();
    world.add(player);
  }

  createBackground() {
    bg = new Background(0, -height/2, color(77, 175, 124));
    this.add(bg);
  }

  add(object) {
    if (object.isDynamic === true) {
      this.dynamicObjects = [...this.dynamicObjects, object];
    } else {
      this.staticObjects = [...this.staticObjects, object];
    }
  }

  createObstacles() {
    this.obstacles = [];

    for (let i = 0; i < NUMBER_OF_OBSTACLES; i++) {
      let obstacle = new Obstacle();

      this.obstacles = [...this.obstacles, obstacle];
    }
  }

  createCoins() {
    this.coins = [];

    for (let i = 0; i < NUMBER_OF_COINS; i++) {
      let coin = new Coin();

      this.coins = [...this.coins, coin];
    }
  }

  sumForces() {
    let sum = {
      x: 0,
      y: 0,
    }

    for (let force of this.forces) {
      sum.x += force.x;
      sum.y += force.y;
    }

    return sum;
  }

  update() {
    for (let object of this.dynamicObjects) {
      let acceleration = this.sumForces();

      object.update(acceleration);
    }
  }

  drawStaticObjects() {
    for (let object of this.staticObjects) {
      object.draw();
    }
  }

  drawDynamicObjects() {
    for (let object of this.dynamicObjects) {
      object.draw();
    }
  }

  drawObstacles() {
    for (let obstacle of this.obstacles) {
      obstacle.draw();
    }
  }

  drawCoins() {
    for (let coin of this.coins) {
      coin.draw();
    }
  }

  removeCollectedCoin(coin) {
    this.coins = this.coins.filter(arrayCoin => {
      return arrayCoin !== coin;
    });
  }

  draw() {
    gameManager.frame++;
    this.drawStaticObjects();
    this.drawDynamicObjects();
    this.drawObstacles();
    this.drawCoins();
    platform.draw();
  }
}