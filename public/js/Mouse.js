class Mouse {
  static getXAxisChange() {
    return Mouse.getMouseX() - Mouse.previousPosition.x;
  }

  static getYAxisChange() {
    return Mouse.getMouseY() - Mouse.previousPosition.y;
  }

  static getMouseX() {
    return (mouseX - translateX) / camera.zoom + camera.position.x;
  }
  
  static getMouseY() {
    return (-1)*(mouseY - translateY) / camera.zoom + camera.position.y;
  }

  static get projection() {
    return {
      x: (this.getMouseX() + camera.position.x) * camera.zoom,
      y: (this.getMouseY() + camera.position.y) * camera.zoom
    }
  }

  static IsMousePressed = false;

  static previousPosition = {
    x: 0,
    y: 0
  }

  static setPreviousPositionToCurrentPosition() {
    Mouse.previousPosition.x = Mouse.getMouseX();
    Mouse.previousPosition.y = Mouse.getMouseY();
  }
}

