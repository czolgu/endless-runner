class Platform {
  constructor() {
    this.position = {
      x: 0,
      y: 0
    }

    this.size = {
      length: random(MIN_PLATFORM_LENGTH, MAX_PLATFORM_LENGTH),
    }

    this.isDynamic = false;
  }

  setRandomLength() {
    this.size.length = random(MIN_PLATFORM_LENGTH, MAX_PLATFORM_LENGTH);
  }

  get projection() {
    return {
      y: (this.position.y - camera.position.y) * camera.zoom,
    }
  }

  draw() {
    fill(68, 156, 114);
    rect(0, this.projection.y - height/4, width, height/2);
  }
}