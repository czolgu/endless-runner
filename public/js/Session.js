class Session {
  constructor() {
    this.validated = false;
  }

  validate() {
    let sessionId = BrowserCookies.get(BrowserCookies.SESSION_COOKIE_NAME);

    socket.emit('validate', {
        'session_id': sessionId
    }, this.handleValidateResponse);
  }

 handleValidateResponse(response) {
    if (response.status === 'updated') {
      session.handleSessionUpdated(response);
    } else if (response.status === 'created') {
      session.handleSessionCreated(response);
    }

    this.validated = true;
    gameManager.gameStart();
  }

  handleSessionUpdated(response) {
    gameManager.privateKey = response.private_key;
  }

  handleSessionCreated(response) {
    BrowserCookies.set(BrowserCookies.SESSION_COOKIE_NAME, response.session_id, 7);
    gameManager.privateKey = response.private_key;
  }
}