class Camera {
  constructor() {
    this.position = {
      x: CAMERA_START_POSITION_X,
      y: CAMERA_START_POSITION_Y
    }

    this.zoom = CAMERA_START_ZOOM;

    this.zoomingSpeed = CAMERA_ZOOMING_SPEED;
    this.maxZoom = CAMERA_MAX_ZOOM;
    this.minZoom = CAMERA_MIN_ZOOM;
  }

  handleScroll(delta) {
      if (delta > 0) {
        this.zoomOut();
      } else {
        this.zoomIn();
      }
  }

  zoomIn() {
    this.zoom += this.zoomingSpeed;

    if (this.zoom >= this.maxZoom) {
      this.zoom = this.maxZoom;
    }
  }

  zoomOut() {
    this.zoom -= this.zoomingSpeed;

    if (this.zoom <= this.minZoom) {
      this.zoom = this.minZoom;
    } 
  }

  center() {
    this.position.x = 0;
    this.position.y = 0;
  }

  setPosition(x, y) {
    this.position.x = x;
    this.position.y = y;
  }

  move(displacement) {
    this.position.x += displacement.x;
    this.position.y += displacement.y;
  }
}