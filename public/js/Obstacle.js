class Obstacle {
  constructor() {
    this.topLeftPoint = {
      x: undefined,
      y: undefined
    }
  
    this.bottomRightPoint = {
      x: undefined,
      y: undefined
    }
  
    this.size = {
      width: random(OBSTACLE_MIN_WIDTH, OBSTACLE_MAX_WIDTH),
      height: random(OBSTACLE_MIN_HEIGHT, OBSTACLE_MAX_HEIGHT),
    }
  
    this.position = {
      x: this.getRandomXPosition(),
      y: this.getYPosition()
    }
  
    this.color = color(random(255), random(255), random(255));
  
    this.isDynamic = false;
 
    this.setEdges();
  }

  getRandomXPosition() {
    let randomX = random(-platform.size.length/2, platform.size.length/2);

    while (this.collidesWithObstacles(randomX) === true) {
      randomX = random(-platform.size.length/2, platform.size.length/2);
    }

    return randomX;
  }

  getYPosition() {
    return this.size.height/2 + (random() > 0.5 ? player.size.height + 10 : 0);
  }

  collidesWithObstacles(randomX) {
    for (let obstacle of world.obstacles) {
      if (this.isInRadius(randomX, obstacle) === true) {
        return true;
      }
    }

    return false;
  }

  isInRadius(randomX, obstacle) {
    return randomX > obstacle.position.x - obstacle.size.width/2 - OBSTACLE_NO_OBSTACLES_ZONE_WIDTH && randomX < obstacle.position.x + obstacle.size.width/2 + OBSTACLE_NO_OBSTACLES_ZONE_WIDTH;
  }

  setEdges() {
    this.topLeftPoint.x = this.position.x - this.size.width/2;
    this.topLeftPoint.y = this.position.y + this.size.height/2;
   
    this.bottomRightPoint.x = this.position.x + this.size.width/2;
    this.bottomRightPoint.y = this.position.y - this.size.height/2;
  }

  get projection() {
    return {
      x: (this.position.x - camera.position.x) * camera.zoom,
      y: (this.position.y - camera.position.y) * camera.zoom,
      width: this.size.width * camera.zoom,
      height: this.size.height * camera.zoom,
    }
  }

  draw() {
    fill(this.color);
    rect(this.projection.x, this.projection.y, this.projection.width, this.projection.height);
  }
}