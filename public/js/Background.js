class Background {
  constructor(x, y, c) {
    this.position = {
      x: x,
      y: y,
    }

    this.color = c;

    this.isDynamic = false;
  }

  draw() {
    fill(this.color);
    rect(this.position.x, this.position.y - (camera.position.y * 0.3), width, height);
  }
}