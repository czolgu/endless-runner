class Coin {
  constructor() {
    this.size = {
      radius: COIN_RADIUS
    }
  
    this.type = random() > 0.5 ? 'speedUp' : 'slowDown';
  
    this.position = {
      x: this.getRandomXPosition(),
      y: this.size.radius/2 + COIN_HEIGHT_ABOVE_GROUND
    } 
  
    this.color = this.type === 'speedUp' ? color(255, 0, 0) : color(0, 255, 0);
  }

  getRandomXPosition() {
    let randomX = random(-platform.size.length/2, platform.size.length/2);

    while(this.collidesWithObstacles(randomX) === true) {
      randomX = random(-platform.size.length/2, platform.size.length/2);
    }

    return randomX;
  }

  collidesWithObstacles(randomX) {
    for (let obstacle of world.obstacles) {
      if (this.isInObstacleRadius(randomX, obstacle) === true) {
        return true;
      }
    }

    return false;
  }

  isInObstacleRadius(randomX, obstacle) {
    return randomX > obstacle.position.x - obstacle.size.width/2 - OBSTACLE_NO_COINS_ZONE_WIDTH && randomX < obstacle.position.x + obstacle.size.width/2 + OBSTACLE_NO_COINS_ZONE_WIDTH;
  }

  get projection() {
    return {
      x: (this.position.x - camera.position.x) * camera.zoom,
      y: (this.position.y - camera.position.y) * camera.zoom,
      radius: this.size.radius * camera.zoom,
    }
  }

  draw() {
    fill(this.color);
    ellipse(this.projection.x, this.projection.y, this.projection.radius);
  }
}