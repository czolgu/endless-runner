class Controls {
  static keyBindings = {
    pauseKey: 'p',
    jumpKey: ' ',
  }
  
  static action(key) {
    if (key === Controls.keyBindings.pauseKey) {
      gameManager.pause = gameManager.pause !== true;
      
      return;
    }

    if (key === Controls.keyBindings.jumpKey) {
      player.jump();

      return;
    }
  }
}