/* TODO
*/

let gameManager;
let session;
let canvas;
let canvasWidth;
let canvasHeight;
let previousFrames = 0;
let translateX;
let translateY;
let world;
let platform;
let bg;
let camera;
let player;
let gravity;
let socket;

const FRAMES_NUMBER_PER_SCORE_POINT = 5;

const CAMERA_MAX_ZOOM = 2;
const CAMERA_MIN_ZOOM = 0.1;
const CAMERA_START_ZOOM = 1;
const CAMERA_ZOOMING_SPEED = 0.03;
const CAMERA_START_POSITION_X = 0;
const CAMERA_START_POSITION_Y = 0;

const BACKGROUND_COLOR = '#bbebfa';
const TARGET_FRAMERATE = 144;
const ZOOMING_SPEED = 0.1;
const GRAVITY_ACCELERATION = -0.1;

const NUMBER_OF_COINS = 1;
const COIN_RADIUS = 10;
const COIN_HEIGHT_ABOVE_GROUND = 10;

const OBSTACLE_MIN_WIDTH = 20;
const OBSTACLE_MAX_WIDTH = 130;
const OBSTACLE_MIN_HEIGHT = 50; 
const OBSTACLE_MAX_HEIGHT = 100;
const OBSTACLE_NO_COINS_ZONE_WIDTH = 100;
const OBSTACLE_NO_OBSTACLES_ZONE_WIDTH = 450;

const NUMBER_OF_OBSTACLES = 1;
const MIN_PLATFORM_LENGTH = OBSTACLE_NO_OBSTACLES_ZONE_WIDTH * NUMBER_OF_OBSTACLES; 
const MAX_PLATFORM_LENGTH = MIN_PLATFORM_LENGTH * 5;

const PLAYER_ACCELERATION = 0.5;
const PLAYER_START_VELOCITY_X = 4;
const PLAYER_START_VELOCITY_Y = 0;
const PLAYER_MIN_VELOCITY_X = PLAYER_START_VELOCITY_X;
const PLAYER_MAX_VELOCITY_X = 12;  
const PLAYER_WIDTH = 30;
const PLAYER_HEIGHT = 30;
const PLAYER_COIN_ACCELERATION_MULTIPLIER = 5;
const PLAYER_COIN_DEACCELERATION_MULTIPLIER = 5; 

function setup() {
    gameManager = new GameManager();
    session = new Session();
    CanvasManager.prepareCanvas();
    frameRate(TARGET_FRAMERATE);
    noStroke();

    translateX = width/2;
    translateY = height/2;

    world.setup();
}

function mouseWheel(e) {
    camera.handleScroll(e.delta);
}

function draw() {
    gameManager.calculatefps();
    rectMode(CENTER);
    background(BACKGROUND_COLOR);
    translate(translateX, translateY);
    scale(1, -1);

    if (gameManager.pause === false) {
        world.update();
        gameManager.updateScore();
    }

    world.draw();
}

$(document).ready(() => {
    socket = io.connect('localhost');

    session.validate();

    $(document).mousemove(() => {
        if (Mouse.IsMousePressed === true) {
            let changeX = (-1)*Mouse.getXAxisChange();
            let changeY = (-1)*Mouse.getYAxisChange();

            camera.move({
                x: changeX,
                y: changeY
            });

            Mouse.setPreviousPositionToCurrentPosition();
        }
    });
    
    $(document).mousedown(()=>{
        Mouse.IsMousePressed = true;
        Mouse.setPreviousPositionToCurrentPosition();
    });
    
    $(document).mouseup(()=>{
        Mouse.IsMousePressed = false;
        Mouse.setPreviousPositionToCurrentPosition();
    });

    $(document).keypress((e)=> {
        Controls.action(e.key);
    });
});

