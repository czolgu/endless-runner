class GameManager {
  constructor() {
    camera = new Camera();
    world = new World();

    this.pause = true;
    this.highscore = 0;
    this.score = 0;
    this.level = 1;
    this.frame = 0;
    this.drawCollisionLines = false;
    this.cameraFollowsPlayer = true;
    this.playerChecksCollisions = true;
    this.privateKey;
  }

  gameStart() {
    this.getHighscore();
    this.pause = false;
  }

  gameEnd() {
    this.saveScore();
    this.pause = true;
    alert(`you died, your score: ${this.score}`);
  }

  updateScore() {
    if (this.frame % FRAMES_NUMBER_PER_SCORE_POINT === 0) {
      this.score++;
      this.updateScoreCounter();
      
      if (this.score > this.highscore) {
        this.updateHighscoreContainer(this.score);
      }
    }
  }

  updateScoreCounter() {
    $('.score-counter').html(`${this.score}`);
  }

  updateLevelNumber() {
    this.level++;
  }

  levelCompleted() {
    this.updateLevelNumber();
    player.speedUp();
    platform.setRandomLength();
    player.moveToStartingPoint();
    world.createObstacles();
    world.createCoins();

    this.saveScore();
  }

  saveScore() {
    socket.emit('save_score', {
      score: this.score,
      private_key: this.privateKey
    }, this.handleSaveScoreResponse);
  }

  handleSaveScoreResponse(data) {
    if (data.status === true) {
      console.log(data.message);
    } else {
      console.log(data.error);
      alert(data.error);
    }
  }

  getHighscore() {
    socket.emit('get_highscore', {
      private_key: this.privateKey
    }, this.handleGetHighscoreResponse);
  }

  handleGetHighscoreResponse(data) {
    if (data.status === true) {
      gameManager.setHighscore(data.highscore);
    } else {
      console.log(data.error);
      alert(data.error);
    }
  }

  setHighscore(highscore) {
    this.highscore = highscore;
    this.updateHighscoreContainer(highscore);
  }

  updateHighscoreContainer(highscore) {
    $('.highscore-container').html(`highscore: ${highscore}`);
  }

  calculatefps() {
    let currentFrames = performance.now();
    let difference = currentFrames - this.previousFrames;
    $('.fps-counter').html(`${(1000/difference).toFixed(1)} updates per second`);
    this.previousFrames = currentFrames;
  }
}