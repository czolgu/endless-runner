let express = require('express');
let app = express();
let server = require('http').Server(app);
let io = require('socket.io')(server);

const PORT = process.env.PORT || 80;
const PRIVATE_KEY_VALIDATION_FAILED_MESSAGE = 'action not allowed';

server.listen(PORT, () => { console.log(`Server listening on port ${PORT}`); });

let highscore = 0;
let currentScore = 0;
let currentSessionId;
let privateKey;

io.on('connection', socket => {
  console.log(`socket of id: ${socket.id} just connected`);

  socket.on('save_score', (data, callback) => {
    if (isPrivateKeyValid(data.private_key) === false) {
      callback({
        status: false,
        error: PRIVATE_KEY_VALIDATION_FAILED_MESSAGE
      });

      return;
    }

    updateScore(data.score);

    callback({
      status: true,
      message: 'Successfully saved the score'
    });

    return;
  });

  const updateScore = score => {
    let clientScore = score;
    currentScore = clientScore;

    if (clientScore > highscore) {
      highscore = clientScore;
    }
  }

  socket.on('get_highscore', (data, callback) => {
    if (isPrivateKeyValid(data.private_key) === false) {
      callback({
        status: false,
        error: PRIVATE_KEY_VALIDATION_FAILED_MESSAGE
      });

      return;
    }

    callback({
      status: true,
      highscore: highscore
    });

    return;
  });

  socket.on('validate', (data, callback) => {
    validateUserById(data.session_id, callback);
  });
});

const isPrivateKeyValid = (requestPrivateKey) => {
  return requestPrivateKey === privateKey;
}

const validateUserById = (sessionId, callback) => {
  if (sessionId === '') {
    createNewSession(callback);

    return;
  }
    
  if (isSessionIdValid(sessionId) === true) {
    updateSession(callback);
  } else {
    createNewSession(callback);
  }
}

const isSessionIdValid = sessionId => {
  return sessionId === currentSessionId;
}

const updateSession = callback => {
  privateKey = createPrivateKey();

  callback({
    status: 'updated',
    message: 'You were already signed',
    private_key: privateKey
  });
}

const createNewSession = callback => {
  currentSessionId = createSessionId();
  privateKey = createPrivateKey();

  callback({
    status: 'created',
    message: 'You are now signed',
    session_id: currentSessionId,
    private_key: privateKey
  });
}

const createPrivateKey = () => {
  let date = new Date().getTime();
  let privateKey = 'xxxxxxxxxxxxxxxxxxxx'.replace(/x/g, char => {
      let number = (date + Math.random()*16) % 16 | 0;
      date = Math.floor(date/16);
      return (char === 'x' ? number : '-').toString(16);
  });
  return privateKey;
}

const createSessionId = () => {
  let date = new Date().getTime();
  let sessionId = 'xxxxxx-xxxxxx-xxxxxx'.replace(/x/g, char => {
      let number = (date + Math.random()*16) % 16 | 0;
      date = Math.floor(date/16);
      return (char === 'x' ? number : '-').toString(16);
  });
  return sessionId;
}

app.use(express.static('public'));